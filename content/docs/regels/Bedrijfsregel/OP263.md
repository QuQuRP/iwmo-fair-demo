---
regel:
  code: OP263
  documentatie: Wanneer gemeenten en aanbieders kiezen voor een outputgerichte werkwijze
    moeten er duidelijke afspraken worden gemaakt over wat precies wordt verstaan
    onder de gewenste output en hoe kan worden vastgesteld hoe de output is behaald.
  type: Bedrijfsregel

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

