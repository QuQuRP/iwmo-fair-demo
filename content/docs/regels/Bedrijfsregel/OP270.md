---
regel:
  code: OP270
  documentatie: Het gebruik van de regieberichten is voor de uitvoeringsvarianten
    inspannings- en outputgericht verplicht (met uitzondering van voor de Gecertificeerde
    Instellingen). Gemeente en aanbieder maken onderling afspraken welke datums gehanteerd
    moeten worden om de start en stop van de levering door te geven. Deze afspraak
    kan per product afwijken, maar dit heeft niet de voorkeur.
  type: Bedrijfsregel

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

