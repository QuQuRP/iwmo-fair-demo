---
regel:
  code: OP043
  documentatie: Een toewijzing eindigt (uiterlijk) op de dag voorafgaand aan de ingangsdatum
    van het PGB.
  type: Bedrijfsregel

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

