---
regel:
  code: OP179
  documentatie: 'Deze regel wordt niet naar de techniek vertaald; daar waar grotere
    bestanden verwerkt kunnen worden is dat uiteraard toegestaan.

    Daar waar de grootte van bestanden tot problemen in de verwerking leidt, is deze
    regel bedoeld om duidelijk te maken dat de verzender de bestandsgrootte moet aanpassen.'
  type: Bedrijfsregel

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

