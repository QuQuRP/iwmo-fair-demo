---
regel:
  code: IV093
  documentatie: "Als een toewijzingbericht wordt gestuurd als gevolg van de honorering\
    \ van een verzoek om wijziging, dan bevat deze alle in het verzoek gevraagde wijzigingen.\
    \ \nEr mag geen mix worden gemaakt van wel/niet toegewezen producten, alles wordt\
    \ conform de aanvraag toegewezen. Indien de GewensteIngangsdatum al gepasseerd\
    \ is, dan kan in overeenstemming met de aanbieder de wijziging hierop aangepast\
    \ worden, dit betreft alleen de ingangsdatum en einddatum van de toewijzingen\n\
    \nAlle ToegewezenProducten in het bericht, ook de ongewijzigde, krijgen dezelfde\
    \ ReferentieAanbieder uit het verzoek om wijziging bericht, omdat het een samenhangend\
    \ geheel van ToegewezenProducten is, dat altijd als een geheel wordt toegewezen\
    \ op basis van het verzoek.\n\nDe vulling van het toewijzingsbericht is afhankelijk\
    \ van de gevraagde wijzigingen, de soort actuele toewijzing, en het moment van\
    \ toewijzen ten opzichte van de gewenste ingangsdatum in het Verzoek om Wijzing.\n\
    \nOngewijzigdProduct:\nVoor elke actuele toewijzing waarvoor een OngewijzigdProduct\
    \ element is gevuld, wordt in het toewijzingbericht de toewijzing meegestuurd\
    \ met ongewijzigde gegevens. RedenWijzing blijft leeg omdat er niets wijzigt,\
    \ en ReferentieAanbieder wordt overgenomen uit Verzoek om Wijziging.\n\nNieuwProduct:\n\
    Voor elk NieuwProduct element wordt in het toewijzingbericht een toewijzing meegestuurd.\n\
    In deze berichtklasse staan nieuwe producten waarvoor in die periode geen actueel\
    \ ToegewezenProduct is. Voor het aanmaken van de ToegewezenProduct wordt de inhoud\
    \ overgenomen uit het Verzoek om Wijziging. RedenWijzing blijft leeg omdat het\
    \ een nieuw ToegewezenProduct betreft, ReferentieAanbieder wordt overgenomen uit\
    \ Verzoek om Wijziging. \n\nTeWijzigenProduct:\nVoor elke aangevraagde wijziging\
    \ uit TeWijzgenProduct wordt een toewijzing (of twee toewijzingen) opgesteld volgens\
    \ onderstaande voorwaarden, conform OP33x1 en OP257. \n\nActuele toewijzing heeft\
    \ budget of frequentie 'totaal binnen geldigheidsduur toewijzing' \nWanneer een\
    \ actuele toewijzing moet wijzigen van het type budget of frequentie 'totaal binnen\
    \ geldigheidsduur toewijzing', dan betreft dit altijd een wijziging vanaf de originele\
    \ begindatum. \nHet budget of de totale omvang kan gewijzigd zijn, en de einddatum\
    \ kan zijn aangepast, maar Toewijzingnummer, Product (of geen Product bij generieke\
    \ toewijzing) en Begindatum blijven gelijk.\n\nActuele toewijzing heeft frequentie\
    \ anders dan 'totaal binnen geldigheidsduut toewijzing'.\nWanneer een wijziging\
    \ is gewenst voor dit type toewijzing, dan is het afhankelijk van de wijziging\
    \ of er een nieuwe toewijzing nodig is, of dat de bestaande toewijzing kan worden\
    \ gewijzigd.\nBestaande regels OP33x1 en OP257 geven aan, dat bij het wijzigen\
    \ van all\xE9\xE9n de einddatum de bestaande toewijzing kan worden aangepast.\
    \ In de andere gevallen, dus als de omvang wijzigt, dan moet de actuele toewijzing\
    \ beeindigd worden en aansluitend een nieuwe toewijzing met de nieuwe omvang toegewezen\
    \ worden.\nDe te gebruiken begin- en einddatum hierbij is afhankelijk van het\
    \ moment van toewijzen, zie hieronder. \n\nGewensteIngangsdatum in de toekomst\
    \ t.o.v. moment van toewijzen. \nIndien het moment van toewijzen voor de gewenste\
    \ ingangsdatum van de wijziging ligt, dan worden de velden uit het verzoek om\
    \ wijziging ongewijzigd overgenomen.\n\nProducten in berichtklasse OngewijzigdProduct\
    \ \nDeze bevat alleen een Toewijzingsnummer, dit refereert naar een actuele toewijzing\
    \ die ongewijzigd voortgezet moet worden. In het toewijzingsbericht wordt deze\
    \ dan ook ongewijzigd opgenomen, alleen de ReferentieAanbieder wordt overgenomen\
    \ uit het Verzoek om Wijziging. Het element RedenWijziging blijft leeg.\n\nProducten\
    \ in berichtklasse TeWijzigenProduct \nHierin staan producten waarvoor een actueel\
    \ ToegewezenProduct is, maar waarvoor nu een wijziging wordt gevraagd. Afhankelijk\
    \ van wat er als wijziging gevraagd wordt zijn er twee verschillende wijzen waarop\
    \ dit in het toewijzingbericht wordt opgenomen (zie ook OP257).\n* Wanneer aan\
    \ de voorwaarden van OP257 wordt voldaan, kan het actueel ToegewezenProduct worden\
    \ aangepast en opnieuw worden verstuurd. De toewijzing houdt hetzelfde ToewijzingNummer.\
    \ Wel wordt de ReferentieAanbieder overgenomen uit het Verzoek om Wijziging en\
    \ de RedenWijziging wordt gevuld met geinitieerd door aanbieder. Dit geldt voor\
    \ wijzigingen van de Einddatum (intrekken of oprekken), wijzigingen van het Maximaal\
    \ Budget en bij wijzigingen van het Volume indien de Frequentie de waarde totaal\
    \ binnen geldigheidsduur toewijzing heeft.\n* In de andere gevallen kan geen aangepast\
    \ ToegewezenProduct worden verstuurd, en wordt de wijziging gerealiseerd in twee\
    \ gedeelten: het actueel ToegewezenProduct wordt ingetrokken; krijgt een einddatum\
    \ GewensteIngangsdatum minus 1 dag. RedenWijziging wordt gevuld met geinitieerd\
    \ door aanbieder en de ReferentieAanbieder wordt overgenomen uit het Verzoek om\
    \ Wijziging. \nDaarnaast wordt er een nieuw ToegewezenProduct gestuurd met als\
    \ ingangsdatum de GewensteIngangsdatum en verder de gevraagde Einddatum, Omvang\
    \ en/of Budget. ReferentieAanbieder wordt overgenomen uit het Verzoek om Wijziging,\
    \ RedenWijziging blijft leeg omdat het een nieuwe toewijzing is. Zie voorbeeld\
    \ 1 hieronder. \n\n\nGewensteIngangsdatum ligt in het verleden t.o.v. moment van\
    \ toewijzen   \nAls GewensteIngangsdatum op het moment van toewijzing in het verleden\
    \ ligt, dan wordt buiten het berichtenverkeer met de aanbieder afgestemd of het\
    \ mogelijk is dat de toewijzing met terugwerkende kracht gedaan wordt.\nAls dat\
    \ het geval is en geen rechtmatigheidsproblemen oplevert, dan wordt toegewezen\
    \ zoals hiervoor, en wordt GewensteIngangsdatum uit het verzoek gebruikt.\nAls\
    \ dat niet kan, dan wordt afgestemd op welke manier de samenhang in het pakket\
    \ van ToegewezenProducten kan worden gehandhaafd, door het kiezen van de juiste\
    \ datum waarop de wijzigingen in gaan, zie de voorbeelden 2 en 3 hieronder. \n\
    \nVoorbeeld 1\nLopend ToegewezenProduct is \n* ToewijzingNummer 123001, Product\
    \ E, 4 uur/week, Ingangsdatum 1 januari 2020, Einddatum 31 december 2020 \n* ToewijzingNummer\
    \ 123002, Product F, 3 uur/week, Ingangsdatum 1 januari 2020, Einddatum 31 december\
    \ 2020\n\nMedio februari 2020 wordt een wijziging aangevraagd met daarin ReferentieAanbieder\
    \ A001\n* TeWijzigenProduct: ToewijzingNummer123001, Product E, 4 uur/week, GewensteIngangsdatum\
    \ 1 januari 2020,  Einddatum 30 juni 2021\n* TeWijzigenProduct: ToewijzingNummer123002,\
    \ Product F, 6 uur/week,  GewensteIngangsdatum 15 maart 2020\n\nDe gemeente ontvangt\
    \ het verzoek, en besluit om het verzoek te honoreren, en doet dit tijdig, door\
    \ op 2 maart 2020 het toewijzingbericht te sturen.\nDe wijziging voor product\
    \ E kan met een gewijzigd ToegewezenProduct gedaan worden, omdat wordt voldaan\
    \ aan OP33x1.\nDe wijziging van product F wordt gedaan door het lopende ToegewezenProduct\
    \ per 14 maart in te trekken, en aansluitend een nieuw ToegewezenProduct met het\
    \ nieuwe volume te geven met een startdatum van 15 maart 2020.\n\nHet toewijzingbericht\
    \ bevat dus:\nToewijzingNummer 123001, Product E, 4 uur/week, Einddatum 30 juni\
    \ 2021,\nReferentieAanbieder A001, RedenWijziging is geinitieerd door aanbieder\n\
    ToewijzingNummer 123002, Product F, 3 uur/week, Einddatum 14 maart 2020\nReferentieAanbieder\
    \ A001, RedenWijziging is geinitieerd door aanbieder\nToewijzingNummer 123078,\
    \ Product F, 6 uur/week, startdatum 15 maart 2020, Einddatum 31 december 2020,\
    \ ReferentieAanbieder A001, RedenWijziging is leeg\n\nVoorbeeld 2\nLopende toewijzing\
    \ is:\n* ToewijzingNummer 345001, Product A, Ingangsdatum 1 januari 2020, Einddatum\
    \ 31 december 2020\n* ToewijzingNummer 345002, Product B, Ingangsdatum 1 januari\
    \ 2020, Einddatum 31 december 2020\n\nMedio februari 2020 wordt een wijziging\
    \ aangevraagd met daarin:\n* Ongewijzigd laten van Toewijzing 345001\n* Wijziging:\
    \ ToewijzingNummer 345002, product B, GewensteIngangsdatum 1 januari 2020, Einddatum\
    \ 15 maart  2020\n* Starten van nieuw product C per 16 maart  2020\nDe toewijzing\
    \ wordt gedaan per 4 april 2020, omdat de gemeente eerst onderzoek moest doen.\
    \ Dan wordt afgestemd wat er moet gebeuren, en welke datum moet worden gekozen.\n\
    \nSituatie a: product B en product C zijn aansluitend nodig, en de aanbieder is\
    \ doorgegaan met leveren van A en B.\nDan kan worden afgesproken dat de toewijzing\
    \ wordt gestuurd met andere maar nog steeds aansluitende datums per 4 april 2020\n\
    * Product A blijft ongewijzigd\n* Product B krijgt een toegewezen einddatum 3\
    \ april 2020\n* Product C krijgt een toegewezen ingangsdatum 4 april 2020\n\n\
    Situatie b: product B en product C zijn aansluitend nodig, en de aanbieder is\
    \ op 16 maart 2020 overgeschakeld naar het leveren van A en C.\nDan kan worden\
    \ afgesproken dat de toewijzing wordt gestuurd zoals in het verzoek, met terugwerkende\
    \ kracht\n* Product A blijft ongewijzigd\n* Product B krijgt een toegewezen einddatum\
    \ 15 maart 2020\n* Product C krijgt een toegewezen ingangsdatum 16 maart 2020\n\
    NB: Het overschakelen naar product C gebeurt terwijl er op dat moment nog geen\
    \ rechtmatigheid is. Dit zal in afstemming met de gemeente moeten gebeuren om\
    \ problemen rond declaratie te voorkomen.\n\nVoorbeeld 3\nLopende toewijzing is:\n\
    * ToewijzingNummer 678001, Product X, Ingangsdatum 1 januari 2019, einddatum 15\
    \ maart  2020\n\nMedio februari 2020 wordt een wijziging aangevraagd met daarin:\n\
    * Ongewijzigd laten ToewijzingNummer 678001\n* Nieuw product Y, Ingangsdatum 16\
    \ maart 2020\nDe toewijzing wordt gedaan per 4 april 2020, omdat de gemeente eerst\
    \ onderzoek moest doen.\n\nSituatie a: product X en product Y zijn aansluitend\
    \ nodig, en de aanbieder is op 16 maart 2020 gestart met het leveren van Y. Dan\
    \ kan worden afgesproken dat de toewijzing wordt gestuurd met terugwerkende kracht\
    \ zoals in het verzoek.\n* Product X blijft ongewijzigd met einddatum 15 maart\
    \ 2020\n* Product Y krijgt een toegewezen ingangsdatum 16 maart 2020\nNB: Het\
    \ starten met leveren van product Y gebeurt terwijl er op dat moment nog geen\
    \ rechtmatigheid is. Dit zal in afstemming met de gemeente moeten gebeuren om\
    \ problemen rond declaratie te voorkomen.\n\nSituatie b: product X en product\
    \ Y zijn niet strikt noodzakelijk aansluitend, en door het uitblijven van de toewijzing\
    \ is de aanbieder gestopt met leveren per 15 maart 2020.\nDan kan worden afgesproken\
    \ dat de toewijzing wordt gestuurd zonder terugwerkende kracht en zonder aansluiting\
    \ in de opvolgende toewijzing\n* Product X blijft ongewijzigd met einddatum 15\
    \ maart 2020\n* Product Y krijgt een toegewezen ingangsdatum 4 april 2020\n\n"
  type: Invulinstructie

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

