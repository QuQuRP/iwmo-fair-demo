---
regel:
  code: IV085
  documentatie: "Wanneer een eerder ingediende prestatie door de aanbieder onjuist\
    \ is bevonden, kan deze door de aanbieder worden ingetrokken door aanlevering\
    \ van een credit prestatie. \nDit kan alleen nadat er een declaratie-antwoordbericht\
    \ op de debet prestatie is ontvangen en de prestatie daarin niet is afgewezen.\
    \ Een door de gemeente afgewezen prestatie wordt nooit gecrediteerd. \n\nNB: een\
    \ creditering  is een eenzijdige handeling van de aanbieder, die niet bestreden\
    \ kan worden door een gemeente. De gemeente kan een ingediende credit prestatie\
    \ dus niet afwijzen, tenzij het technische afkeur (XSLT of XSD) betreft. Zie hiervoor\
    \ invulinstructies IV088 en IV087). "
  type: Invulinstructie

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

