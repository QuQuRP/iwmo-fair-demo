---
regel:
  code: CS089
  documentatie: "Concreet betekent dit dat de waarde moet voldoen aan de reguliere\
    \ expressie: [1-9][0-9]{3}[a-zA-Z]{2}. \nZie ook https://www.postcode.nl/."
  type: Constraint

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

