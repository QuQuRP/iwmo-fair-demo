---
regel:
  code: RS033
  documentatie: 'Ten behoeve van het uitsluiten van empty elements in het xml-bericht.


    Bij optionele elementen van het type string is het in xml toegestaan om het element
    leeg op te nemen in een bericht. Doormiddel van het pattern is dit niet meer toegestaan
    en moet er altijd vulling zijn anders dan spaties.'
  type: Constraint

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

