---
regel:
  code: TR349
  documentatie: "Het verzoek om wijziging bericht bevat alle actuele toegewezen producten\
    \ van de client, hetzij in OngewijzigdProduct, hetzij in TeWijzigenProduct\nActuele\
    \ toewijzingen zijn toewijzingen die op of na de huidige datum geldig zijn, of\
    \ waarvan de ingangsdatum in de toekomst ligt. \nAlle actuele toewijzingen zijn\
    \ terug te vinden, hetzij als OngewijzigdProduct, hetzij als TeWijzigenProduct.\n"
  type: Technische regel

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

