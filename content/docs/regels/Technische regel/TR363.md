---
regel:
  code: TR363
  documentatie: Elke toewijzing die op basis van het verzoek om wijziging wordt gegeven
    krijgt dezelfde ReferentieAanbieder, die wordt overgenomen uit het verzoek
  type: Technische regel

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

