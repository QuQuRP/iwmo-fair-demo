---
regel:
  code: TR365
  documentatie: "Er kan slechts 1 maal een antwoordbericht verstuurd worden met in\
    \ VerzoekAntwoord de waarde 2 (Aanvraag in onderzoek). \nDaarna moet er ofwel\
    \ een toewijzing ofwel een antwoordbericht met in VerzoekAntwoord de waarde 1\
    \ (Verzoek afgewezen) worden gestuurd."
  type: Technische regel

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

