---
regel:
  code: TR376
  documentatie: "Bij iedere goed of (deels) afgekeurde Declaratie is het Declaratie-antwoord\
    \ gerelateerd aan een ingediende declaratie o.b.v. DeclaratieNummer. Een goed\
    \ of (deels) afgekeurde declaratie wordt middels een Declaratie-antwoordbericht\
    \ verstuurd en is een functionele/inhoudelijke reactie. \n"
  type: Technische regel

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

