#
# Doel: iStandaard BizzDesign Output (Excels en XSDs) naar YMAL en Markdown Formaat Conversie om:
#       - Daarmee te komen tot een basis Data-Beheer waarmee diverse outputs kunnen worden gegenereerd.
#       - Uiteindelijk te komen tot uitfasering van de huidige BizzDesign (legacy) Applicatie
#
# Status: Verkenning op basis vanSSG HUGO (YAML-Data en Markdown-Content) output
#
# Auteur: A. Haldar (Onno)
#

# Package imports
import os
import sys

# Local imports
from istd_klassen import *
from import_bizz_xsds import *
from import_bizz_xslxs import *
from export_hugo_data import *
from create_hugo_content import *

# Initialisatie
importPath = sys.argv[1]
print('Import Path: ' + importPath)
outputPath = ''
if len(sys.argv) > 2:
    outputPath = sys.argv[2]
print('Output Path: ' + importPath)
berichten: IstdBericht = []
basisSchema: IstdBasisSchema
dataTypen: IstdDataType = []
regels: IstdRegel = []
codeLijsten: IstdCodeLijst = []

# Importeer XSDs
importPathXsds = os.path.join(importPath, 'xsds')
print('importPathXsds: ' + importPathXsds)
for xsdFile in os.listdir(importPathXsds):
    xsdFileTst = xsdFile.lower()
    
    if xsdFileTst.endswith('.xsd'):
        # Alleen XSD-bestanden verwerken
        importXsdFilePath = os.path.join(importPathXsds, xsdFile)

        if not xsdFileTst == 'basisschema.xsd':
            print('Importeer Berichtschema: ' + xsdFile)
            berichten.append(importBerichtXsd(importXsdFilePath))
            
        else :
            print('Importeer BasissSchema en Data Typen')
            basisSchema, dataTypen = importBasisschemaXsd(importXsdFilePath)


# Importeer Excels
berichtenRegels: IstdBericht = []
dataTypenRegels: IstdDataType = []
importPathExcels = os.path.join(importPath, 'excels')

for excelFile in os.listdir(importPathExcels):
    if excelFile.lower().endswith('.xlsx'):
        # Alleen Excel-bestanden
        excelFilePath = os.path.join(importPathExcels, excelFile)

        if excelFile.lower().startswith('regelrapport'):
            print('Importeer Regelrapport')
            berichtenRegels, dataTypenRegels = importRegelsPerBerichtelementWs(excelFilePath)
            regels = importRegelsWs(excelFilePath)
        elif excelFile.lower().startswith('codelijsten'):
            print('Importeer CodeLijsten')
            codeLijsten = importCodelijstenWs(excelFilePath)
        else:
            print('Onbekende Import-Excel ==> ' + excelFile)

# Voeg Regels toe aan Berichten
for bericht in berichten:
    for berichtRegels in berichtenRegels:
        if bericht.naam == berichtRegels.naam:
            # Voeg regels toe op Bericht Niveau
            bericht.regels.extend(berichtRegels.regels)
            for klasse in bericht.klassen:
                for klasseRegels in berichtRegels.klassen:
                    if klasse.naam == klasseRegels.naam:
                        # Voeg regels toe op Klasse Niveau
                        klasse.regels.extend(klasseRegels.regels)
                        for element in klasse.elementen:
                            for elementRegels in klasseRegels.elementen:
                                if element.naam == elementRegels.naam:
                                    # Voeg regels toe op Element Niveau
                                    element.regels.extend(elementRegels.regels)

# Voeg Regels en Codelijst toe aan Data Typen
for dataType in dataTypen:
    for dataTypeRegels in dataTypenRegels:
        if dataType.naam == dataTypeRegels.naam:
            dataType.regels.extend(dataTypeRegels.regels)
            dataType.codeLijst = dataTypeRegels.codeLijst
            

# Exporteer YAML-Data
dataPath = defaultDataPath(outputPath)
print('dataPath = ' + dataPath)
exportBerichtenYamls(berichten, dataPath)
exportBasisSchemaYaml(basisSchema, dataPath)
exportDataTypenYamls(dataTypen, dataPath)
exportRegelYamls(regels, dataPath)
exportCodeLijstYamls(codeLijsten, dataPath)

# Maak Markdown-Content
contentPath = defaultContentDocsPath(os.path.join(outputPath, 'content'))
print('contentPath = ' + contentPath)
createIstdBerichtenHugoBookContent(berichten, contentPath)
createIstdDataTypenHugoBookContent(dataTypen, contentPath)
createIstdRegelsHugoBookContent(regels, contentPath)
createIstdCodeLijstenHugoBookContent(codeLijsten, contentPath)