# package imports
import os

# local imports
from export_hugo_data import exportDataTypenYamls, exportBasisSchemaYaml
from import_bizz_xsds import importBasisschemaXsd

# Interne Bouw test
testInputPath = os.path.join('iwmo-bronnen', 'Release 3.0.4')
testOutputPath = 'test-output'

testBasisSchemaImportFilePath = os.path.join(testInputPath, 'xsds','basisschema.xsd')
testExportPath = os.path.join('test-output', 'data')
basisSchema, dataTypen = importBasisschemaXsd(testBasisSchemaImportFilePath)
print('Test BasisSchema XSD Import')
print('======================================================================================')
print('testBasisSchemaImportFilePath = ' + testBasisSchemaImportFilePath)
print('testExportPath = ' + testExportPath)
print('======================================================================================')
exportBasisSchemaYaml(basisSchema, testExportPath)
exportDataTypenYamls(dataTypen, testExportPath)