# Conversie van iStandaard BizzDesign **Output** naar HUGO Book Publicatie **Input**

## Doel

De **Python-scripts** in deze folder zijn bedoeld als verkenning voor de mogelijke opties hoe de **BizzDesign-output** kan worden gemigreerd naar gestandaarddiseerde **YAML-data** en **Markdown-content** die kan worden gebruikt voor diverse doeleinden. Daarbij is de eerste **MVP** het leveren van **data** en **content** voor een voorbeeld-publicatie.
  

## Overzicht werking scripts

Het conversie-script [convert_ist_bizz_2_hugo](convert_istd_bizz_2_hugo.py) omvat de volledige migratie van de **BizzDesig-output** naar **HUGO Data en Content input**.

```plantuml
@startuml
(*) --> "Importeer XSDs"
--> "Importeer Excels"
--> "Voeg XSD en Excel imports samen"
--> "Exporteer YAML-Data"
--> "Maak Markdown-Content"
--> (*)
@enduml
```


# Gebruikte componenten uit het Eco-systeem

Dit zijn voorlopige keuzes maar wel volledig herbruibare componenten die binnen het `Ecosysteem` veel worden toegepast, breed worden ondersteund en op lange termijn duurzaam vanuit `Open Standaarden` zijn samengesteld.*


## Python

De scripts zijn gebaseerd op een Python 3 omgeving. Zie https://www.python.org.

Lokaal zijn de scripts ontwikkeld op een BREW-omgeving (Mac OSX). Via GitLab-CI wordt gebruik gemaakt van de Alpine-omgeving (Linux). 

De volgende extra te installeren Python Packages zijn benodigd voor de functionaliteit in de scripts:

- [XmlToDict](https://pypi.org/project/xmltodict) voor het kunnen inlezen van de XSD's (berichten)
- [OpenPyXl](https://openpyxl.readthedocs.io/en/stable/) voor het kunnen inlezen van de iStandaarden referentie-sheets (met codelijsten, datatypen en regels)
- [PyYAML](https://pyyaml.org/wiki/PyYAMLDocumentation) voor de formatering van de output als gestandaardiseerde YAML-data
- [Humps](https://humps.readthedocs.io/en/latest/) voor diverse tekst-formatering
- [enum](https://docs.python.org/3/library/enum.html) als hulp-functionaliteit voor de conversie-logica
