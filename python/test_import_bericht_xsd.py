# package imports
import os

# local imports
from export_hugo_data import exportBerichtYaml
from import_bizz_xsds import importBerichtXsd

# Interne Bouw test
testInputPath = os.path.join('iwmo-bronnen', 'Release 3.0.4')
testOutputPath = 'test-output'

# Interne Bouw test
berichtNaam =  'WMO305'
testBerichtImportFilePath = os.path.join(testInputPath, 'xsds', berichtNaam + '.xsd')
testExportPath = os.path.join('test-output', 'data')
bericht = importBerichtXsd(testBerichtImportFilePath)
print('Test ' + berichtNaam + ' XSD Import')
print('======================================================================================')
print('testBerichtImportFilePath = ' + testBerichtImportFilePath)
print('testExportPath = ' + testExportPath)
print('======================================================================================')
exportBerichtYaml(bericht, testExportPath)