# package imports
import os

# local imports
from export_hugo_data import exportCodeLijstYamls
from import_bizz_xslxs import importCodelijstenWs

# Interne Bouw test
testInputPath = os.path.join('iwmo-bronnen', 'Release 3.0.4')
testOutputPath = 'test-output'

testCodeLijstImportFilePath = os.path.join(testInputPath, 'excels','codelijsten-iwmo-3.0.4.xlsx')
testExportPath = os.path.join('test-output', 'data')
#codeLijsten = importCodelijstenWs(testCodeLijstImportFilePath, 'Codelijsten iWmo')
codeLijsten = importCodelijstenWs(testCodeLijstImportFilePath)
print('Test Codelijsten Excel Import')
print('======================================================================================')
print('testCodeLijstImportFilePath = ' + testCodeLijstImportFilePath)
print('testExportPath = ' + testExportPath)
print('======================================================================================')
exportCodeLijstYamls(codeLijsten, testExportPath)