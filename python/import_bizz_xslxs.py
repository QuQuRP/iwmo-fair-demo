#
# iStandaard BizzDesign Output XLSX-bestand (Excel) conversie naar Pyhon Class Formaat (istd_klassen)
#
# Status: Verkenning op basis van huidige (vanuit BizzDesign-scripts) gepubliceerde XSDs
#
# Auteur: A. Haldar (Onno)
#

# package imports
from openpyxl import load_workbook
from enum import Enum
from humps import camelize

# local imports
from istd_klassen import *

# Duiding voor type Kolom
class ColumnType(Enum):
    BERICHT = 0
    KLASSE = 1
    ELEMENT = 2
    DATATYPE = 3
    SLEUTEL = 4
    CODELIJST = 5
    REGEL = 6

def mergeDataType(dataTypen: IstdDataType = [], dataTypeNaam: str = None, elementDataType: str = None):
    """Zoek datatype via naam in datatypen-lijst (en voeg nieuwe toe indien niet aanwezig). Geeft index laatst gemuteerde datatype-item terug."""
    # Zoek datatype
    dataType = None
    dataTypeIndex = 0
    while dataTypeIndex < len(dataTypen) and dataType == None:
        if dataTypen[dataTypeIndex].naam == dataTypeNaam:
            dataType = dataTypen[dataTypeIndex]
        else:
            dataTypeIndex = dataTypeIndex + 1
        
    if dataType == None:
        # Voeg nieuw datatype toe indien niet gevonden
        dataTypen.append(IstdDataType(dataTypeNaam))
        dataTypeIndex = len(dataTypen) - 1

    if elementDataType != None:
        # Zoek element 
        elementDataTypeList = elementDataType.split('\\')
        elementNaam = elementDataTypeList[0]
        element = None
        elementIndex = 0
        while elementIndex < len(dataTypen[dataTypeIndex].elementen) and element == None:
            if dataType.elementen[elementIndex].naam == elementNaam:
                element = dataType.elementen[elementIndex]
            else:
                elementIndex = elementIndex + 1

        if element == None:
            # Voeg nieuw element toe aan datatype indien niet gevonden
            dataTypen[dataTypeIndex].elementen.append(IstdElement(elementNaam))
            elementIndex = len(dataTypen[dataTypeIndex].elementen) - 1

        if len(elementDataTypeList) == 2 and dataTypen[dataTypeIndex].elementen[elementIndex].dataType == None:
            # Voeg nieuwe datatype samen
            dataTypeNaam = elementDataTypeList[1]
            dataTypen[dataTypeIndex].elementen[elementIndex].dataType = dataTypeNaam
            mergeDataType(dataTypen, dataTypeNaam)

    return dataTypeIndex

def importRegelsPerBerichtelementWs(regelsWbFilePath: str, regelsPerBerichtElemWs: str = 'Regels per bericht(element)'):
    """Converteer Sheet 'Regels per bericht(element)' uit 'Regelrapport-Excel' naar Berichten (List / Class Structuur)."""
    wb = load_workbook(filename=regelsWbFilePath)
    ws = wb[regelsPerBerichtElemWs]
    columnNames = []
    rowNr: int = 0
    berichten = []
    dataTypen = []
    lastDataTypeIndex = -1

    # process every row in the worksheet
    for wsRow in ws:

        if rowNr == 0:
            # add column names from the first row
            print('Importeer uit Work Book: "' + regelsWbFilePath + '"')
            print('In Work Sheet: [' + regelsPerBerichtElemWs + '] volgende kolomen gevonden:')
            print('================================================================')

            for wsCell in wsRow:
                columnName: str = camelize(wsCell.value).replace('\\', '')
                print('- ' + columnName)
                columnNames.append(columnName)

        else:
            # next rows with data values
            columnNr = 0
            lastColumnType: ColumnType = None

            # process cell value for every column in the row
            for wsCell in wsRow:
                # mapping van kolomcellen
                columnName: str = columnNames[columnNr]

                if wsCell.value != None:
                    # kolomcel-waarde gevonden om te mappen
                    columnValue: str = wsCell.value
                    berichtIndex = len(berichten) - 1

                    if columnName == 'bericht':

                        if berichtIndex == -1:
                            # eerste bericht
                            berichten.append(IstdBericht(columnValue))
                        elif berichten[berichtIndex].naam != columnValue:
                            # volgende bericht
                            berichten.append(IstdBericht(columnValue))

                        lastColumnType = ColumnType.BERICHT

                    elif columnName == 'berichtklasse':
                        klasseIndex = len(berichten[berichtIndex].klassen) - 1

                        if klasseIndex == -1:
                            # eerste klasse
                            berichten[berichtIndex].klassen.append(IstdKlasse(columnValue))
                        elif berichten[berichtIndex].klassen[klasseIndex].naam != columnValue:
                            # volgende klasse
                            berichten[berichtIndex].klassen.append(IstdKlasse(columnValue))

                        lastColumnType = ColumnType.KLASSE

                    elif columnName == 'berichtelementDatatype':
                        klasseIndex = len(berichten[berichtIndex].klassen) - 1
                        elementIndex = len(berichten[berichtIndex].klassen[klasseIndex].elementen) - 1
                        columnValueList = columnValue.split('\\')
                        elementValue = columnValueList[0]

                        if elementIndex == -1:
                            # eerste element
                            berichten[berichtIndex].klassen[klasseIndex].elementen.append(IstdElement(elementValue))
                            elementIndex = elementIndex + 1
                        elif berichten[berichtIndex].klassen[klasseIndex].elementen[elementIndex].naam != elementValue:
                            # volgende element
                            berichten[berichtIndex].klassen[klasseIndex].elementen.append(IstdElement(elementValue))
                            elementIndex = elementIndex + 1

                        if len(columnValueList) > 1:
                            # bevat tevens datatyoe
                            dataTypeNaam = columnValueList[1]
                            # vooeg toe als klasse element datatype
                            berichten[berichtIndex].klassen[klasseIndex].elementen[elementIndex].dataType = dataTypeNaam
                            # samenvoegen met lijst voor datatypen
                            lastDataTypeIndex = mergeDataType(dataTypen, dataTypeNaam)
                            lastColumnType = ColumnType.DATATYPE
                        else:
                            lastColumnType = ColumnType.ELEMENT

                    elif columnName == 'CDTElementDatatype':
                        klasseIndex = len(berichten[berichtIndex].klassen) - 1
                        elementIndex = len(berichten[berichtIndex].klassen[klasseIndex].elementen) - 1
                        lastDataTypeIndex = mergeDataType(dataTypen, berichten[berichtIndex].klassen[klasseIndex].elementen[elementIndex].dataType, columnValue)
                        lastColumnType = ColumnType.DATATYPE
                        
                    elif columnName == 'sleutelelement':
                        klasseIndex = len(berichten[berichtIndex].klassen) - 1
                        elementIndex = len(berichten[berichtIndex].klassen[klasseIndex].elementen) - 1
                        berichten[berichtIndex].klassen[klasseIndex].elementen[elementIndex].sleutel = columnValue == 'ja'
                        lastColumnType = ColumnType.SLEUTEL

                    elif columnName == 'codelijst':
                        dataTypen[lastDataTypeIndex].codeLijst = columnValue
                        lastColumnType = ColumnType.CODELIJST

                    elif columnName == 'regelcode':
                        klasseIndex = len(berichten[berichtIndex].klassen) - 1
                        
                        # het voorgaande type kolom bepaald het niveau waarop regel is gekoppeld
                        if lastColumnType == ColumnType.BERICHT:

                            if not columnValue in berichten[berichtIndex].regels:
                                # voeg regel toe indien aanwezig
                                berichten[berichtIndex].regels.append(columnValue)

                        elif lastColumnType == ColumnType.KLASSE:

                            if not columnValue in berichten[berichtIndex].klassen[klasseIndex].regels:
                                # voeg regel toe indien aanwezig
                                berichten[berichtIndex].klassen[klasseIndex].regels.append(columnValue)

                        elif lastColumnType == ColumnType.ELEMENT:
                            elementIndex = len(berichten[berichtIndex].klassen[klasseIndex].elementen) - 1

                            if not columnValue in berichten[berichtIndex].klassen[klasseIndex].elementen[elementIndex].regels:
                                # voeg regel toe indien aanwezig
                                berichten[berichtIndex].klassen[klasseIndex].elementen[elementIndex].regels.append(columnValue)

                        elif lastColumnType == ColumnType.DATATYPE:
                            
                            if not columnValue in dataTypen[lastDataTypeIndex].regels:
                                # voeg regel toe indien aanwezig
                                dataTypen[lastDataTypeIndex].regels.append(columnValue)

                        lastColumnType = ColumnType.REGEL
                        
                # process next column
                columnNr = columnNr + 1

        # process next row
        rowNr = rowNr + 1

    return berichten, dataTypen

# convert Regel Rapport Excel File to HUGO Markdown Content Files
def importRegelsWs(regelsWbFilePath: str, regelsWs: str = 'Regels'):
    # Load excel worksheet to process
    wb = load_workbook(filename=regelsWbFilePath)
    ws = wb[regelsWs]
    columnNames = []
    regels = []
    rowNr: int = 0

    # process every row in the worksheet
    for wsRow in ws:
        if rowNr < 1:
            # add column names from the first row
            for wsCell in wsRow:
                columnNames.append(camelize(wsCell.value))
        else:
            # get data row values for each Regel
            columnNr = 0
            # create new iStandaard regel instance
            regel = IstdRegel()
            # process cell value for every column in the row
            for wsCell in wsRow:
                # map column cell values to regel instance
                columnName: str = columnNames[columnNr]
                if columnName == 'type':
                    regel.type = wsCell.value
                elif columnName == 'regelcode':
                    regel.code = wsCell.value
                elif columnName == 'titel':
                    regel.titel = wsCell.value
                elif columnName == 'documentatie':
                    regel.documentatie = wsCell.value
                elif columnName == 'XSDRestrictie':
                    regel.XSDRestrictie = wsCell.value
                elif columnName == 'retourcode':
                    regel.retourcode = wsCell.value
                elif columnName.endswith('niveau'):
                    regel.controleniveau = wsCell.value
                elif columnName == 'referentieregel':
                    regel.referentieregel = wsCell.value
                else:
                    print('ColumnName Not Mapped: ==> ' + columnName)
                # next column to process
                columnNr = columnNr + 1
            # add processed regel instance
            regels.append(regel)
        # process next row
        rowNr = rowNr + 1

    return regels


# convert Regel Rapport Excel File to HUGO Markdown Content Files
def importCodelijstenWs(codeLijstWbFilePath: str, codeLijstenWs: str = None):
    # Load excel worksheet to process
    wb = load_workbook(filename=codeLijstWbFilePath)
    
    if codeLijstenWs == None:
        for wsx in wb:
            if wsx.title.startswith('Codelijsten'):
                codeLijstenWs = wsx.title

    ws = wb[codeLijstenWs]
    columnNames = []
    codeLijsten = []
    codeLijstIndex = -1
    rowNr: int = 0

    # process every row in the worksheet
    for wsRow in ws:
        if rowNr < 1:
            # add column names from the first row
            for wsCell in wsRow:
                columnNames.append(camelize(wsCell.value))
        else:
            # get data row values for each Regel
            columnNr = 0
            # create new iStandaard regel instance
            codeItem = IstdCodeItem()
            # process cell value for every column in the row
            for wsCell in wsRow:
                # map column cell values to regel instance
                columnName: str = columnNames[columnNr]
                columnValue: str = wsCell.value

                if columnName == 'codelijst':
                    codeLijstIdList = columnValue.split(':')
                    naam = str(codeLijstIdList[0]).strip()
                    beschrijving = str(codeLijstIdList[1]).strip()

                    if codeLijstIndex == -1:
                        # eerste gevonden codelijst
                        codeLijsten.append(IstdCodeLijst(naam, beschrijving))
                    elif codeLijsten[codeLijstIndex].naam != naam:
                        # overgang naar volgende codelijst
                        codeLijsten.append(IstdCodeLijst(naam, beschrijving))

                    codeLijstIndex = len(codeLijsten) - 1
                elif columnName == 'code':
                    codeItem.code = columnValue
                elif columnName == 'documentatie':
                    codeItem.documentatie = columnValue
                elif columnName == 'mutatiedatum':
                    if columnValue != '':
                        codeItem.mutatiedatum = columnValue
                elif columnName == 'mutatie':
                    if columnValue != '':
                        codeItem.mutatie = columnValue
                elif columnName == 'ingangsdatum':
                    if columnValue != '':
                        codeItem.ingangsdatum = columnValue
                elif columnName == 'expiratiedatum':
                    if columnValue != '':
                        codeItem.expiratiedatum = columnValue
                else:
                    print('ColumnName Not Mapped: ==> ' + columnName)
                    print('columnValue Not Mapped: ==> ' + columnValue)
                # next column to process
                columnNr = columnNr + 1

            if codeLijsten[codeLijstIndex].naam + ': ' + codeLijsten[codeLijstIndex].beschrijving == codeItem.code:
                # first code lijst item
                codeLijsten[codeLijstIndex].documentatie = codeItem.documentatie
            else:
                # next code lijst item
                codeLijsten[codeLijstIndex].codeItems.append(codeItem)

        # process next row
        rowNr = rowNr + 1

    return codeLijsten