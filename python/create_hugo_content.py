#
# Maken van HUGO Markdown-formaat Content vanuit iStandaard-klassen (istd_klassen)
#
# Status: Verkenning op basis van huidige (vanuit BizzDesign-scripts) gepubliceerde XSDs
#
# Auteur: A. Haldar (Onno)
#

# package imports
import os
from datetime import date

# local imports
from istd_klassen import *
from yaml_utils import *

def generatieStempel():
    """Datum Stempel Generatie Content."""
    stempel = '\n** Generereerd door `'
    stempel = stempel + os.path.basename(__file__)
    stempel = stempel + '` op ' + date.today().strftime("%d %B, %Y") + '**\n'
    return  stempel

def buildFrontMatterStr(frontMatterObject: any):
    """Markdown FrontMatter Object Parsing."""
    return '---\n' + buildYamlStr(frontMatterObject) + '---\n'

def buildSectionIndexMdStr(bookCollapseSection: bool = True, title: str = '(titel is leeg)', description: str = '(beschrijving is leeg)'):
    """HUGO Section Index String Parsing."""
    sectionIndexMd = buildFrontMatterStr({
        'bookCollapseSection': bookCollapseSection,
        'title': title,
        'description': description
    })
    sectionIndexMd = sectionIndexMd + generatieStempel() +  '\n{{< sectiontable >}}\n'
    return sectionIndexMd

def defaultContentDocsPath(contentPath: str = 'content'):
    """Standaard HUGO SSG Content Locatie."""
    return os.path.join(contentPath, 'docs')

def createIstdCodeLijstenHugoBookContent(codeLijsten: IstdCodeLijst = [], contentPath: str = defaultContentDocsPath()):
    """Maak Content voor CodeLijsten in Markdown-formaat."""
    # initialisatie
    codeLijstenContentPath = os.path.join(contentPath, 'codelijsten')
    os.makedirs(codeLijstenContentPath, exist_ok=True)
    # CodeLijsten index-MD
    codeLijstenContentIndexFilePath = os.path.join(codeLijstenContentPath, '_index.md')
    with open(codeLijstenContentIndexFilePath, 'w') as codeLijstenContentIndexFile:
        print(buildSectionIndexMdStr(title='CodeLijsten', description='iStandaard CodeLijsten'), file=codeLijstenContentIndexFile)
    # CodeLijst MD-bestanden
    for codeLijst in codeLijsten:
        codeLijstContentPath = os.path.join(codeLijstenContentPath, codeLijst.naam)
        if not os.path.exists(codeLijstContentPath):
            os.makedirs(codeLijstContentPath, exist_ok=False)
            # CodeLijst index-bestand
            codeLijstContentIndexFilePath = os.path.join(codeLijstContentPath, '_index.md')
            with open(codeLijstContentIndexFilePath, 'w') as codeLijstContentIndexFile:
                print(buildFrontMatterStr({
                    'codeLijst': codeLijst.naam,
                    'bookCollapseSection': True
                }), file=codeLijstContentIndexFile)
                print(generatieStempel(), file=codeLijstContentIndexFile)
            # CodeItem md-bestanden
            for codeItem in codeLijst.codeItems:
                codeItemFilePath = os.path.join(codeLijstContentPath, codeItem.code + '.md')
                with open(codeItemFilePath, 'w') as codeItemContentFile:
                    print(buildFrontMatterStr({
                    'codeItem': {
                        'codeLijst': codeLijst.naam, 
                        'code': codeItem.code,
                    }
                    }), file=codeItemContentFile)
                    print(generatieStempel(), file=codeItemContentFile)


def createIstdRegelsHugoBookContent(regels: IstdRegel = [], contentPath: str = defaultContentDocsPath()):
    """Maak Content voor Regels in Markdown-formaat."""
    # initialisatie
    regelContentPath = os.path.join(contentPath, 'regels')
    os.makedirs(regelContentPath, exist_ok=True)
    # Regel index-MD
    regelContentIndexFilePath = os.path.join(regelContentPath, '_index.md')
    with open(regelContentIndexFilePath, 'w') as regelContentIndexFile:
        print(buildSectionIndexMdStr(title='Regels', description='iStandaard Regels'), file=regelContentIndexFile)
    # Type Regel MD-bestanden
    for regel in regels:
        regelTypeContentPath = os.path.join(regelContentPath, regel.type)
        if not os.path.exists(regelTypeContentPath):
            os.makedirs(regelTypeContentPath, exist_ok=False)
            # Regel Type index-bestand
            regelTypeContentIndexFilePath = os.path.join(regelTypeContentPath, '_index.md')
            with open(regelTypeContentIndexFilePath, 'w') as regelTypeContentIndexFile:
                print(buildSectionIndexMdStr(title=regel.type, description='iStandaard Regeltype'), file=regelTypeContentIndexFile)
        # Regel MD-bestand
        regelContentFilePath = os.path.join(regelTypeContentPath, regel.code + '.md')
        with open(regelContentFilePath, 'w') as regelContentFile:
            print(buildFrontMatterStr({
            'regel': {
                'type': regel.type, 
                'code': regel.code,
                'documentatie': regel.documentatie
            }
            }), file=regelContentFile)
            print(generatieStempel(), file=regelContentFile)

def createIstdDataTypenHugoBookContent(dataTypen: IstdDataType = [], contentPath: str = defaultContentDocsPath()):
    """Maak HUGO Book Content voor Data Typen."""
    # initialisatie
    dataTypeContentPath = os.path.join(contentPath, 'datatypen')
    os.makedirs(dataTypeContentPath, exist_ok=True)
    # index-bestand
    dataTypeContentIndexFilePath = os.path.join(dataTypeContentPath, '_index.md')
    with open(dataTypeContentIndexFilePath, 'w') as dataTypeContentIndexFile:
        print(buildSectionIndexMdStr(title='Datatypen', description='iStandaard Gegevensbeschrijvingen'), file=dataTypeContentIndexFile)
        for dataType in dataTypen:
            dataTypeContentFilePath = os.path.join(dataTypeContentPath, dataType.naam + '.md')
            with open(dataTypeContentFilePath, 'w') as contentTypeDataFile:
                print(buildFrontMatterStr({
                'dataType': dataType.naam
                }), file=contentTypeDataFile)
                print(generatieStempel(), file=contentTypeDataFile)

def createIstdElementHugoBookContent(berichtNaam: str, klasseNaam: str, element: IstdElement, ElementenContentPath: str = None):
    """Maak HUGO Book Theme Content voor Element."""
    # initialisatie
    elementContentFilePath = os.path.join(ElementenContentPath, element.naam.lower() + '.md')
    # element-bestand
    with open(elementContentFilePath, 'w') as elementContentFile:
        print(buildFrontMatterStr({
            'element': {
                'bericht': berichtNaam, 
                'klasse': klasseNaam,
                'naam': element.naam
            }
        }), file=elementContentFile)
        print(generatieStempel(), file=elementContentFile)


def createIstdKlasseHugoBookContent(berichtNaam: str, klasse: IstdKlasse, klassenContentPath: str = None):
    """Maak HUGO Book Theme Content voor Klassen en onderliggende Elementen."""
    # initialisatie
    klasseContentDirPath = os.path.join(klassenContentPath, klasse.naam)
    os.makedirs(klasseContentDirPath, exist_ok=True)
    elementenContentDirPath = os.path.join(klasseContentDirPath, 'elementen')
    os.makedirs(elementenContentDirPath, exist_ok=True)
    # klasse-index-bestand
    klasseContentIndexFilePath = os.path.join(klasseContentDirPath, '_index.md')
    with open(klasseContentIndexFilePath, 'w') as klasseContentIndexFile:
        print(buildFrontMatterStr({
            'klasse': {
                'bericht': berichtNaam, 
                'naam': klasse.naam },
            'bookCollapseSection': True
        }), file=klasseContentIndexFile)
        print(generatieStempel(), file=klasseContentIndexFile)
    # elementen-index-bestand
    elementenContentIndexFilePath = os.path.join(elementenContentDirPath, '_index.md')
    with open(elementenContentIndexFilePath, 'w') as elementenContentIndexFile:
        print(buildSectionIndexMdStr(title='Elementen', description=berichtNaam.upper() + ' / ' + klasse.naam), file=elementenContentIndexFile)
    # element-bestanden
    for element in klasse.elementen:
        createIstdElementHugoBookContent(berichtNaam, klasse.naam, element, elementenContentDirPath)


def createIstdBerichtHugoBookContent(bericht: IstdBericht, berichtContentPath: str = None):
    """Maak HUGO Book Theme Content voor Bericht en onderliggende Klassen en Elementen."""
    # initialisatie
    berichtContentDirPath = os.path.join(berichtContentPath, bericht.naam)
    os.makedirs(berichtContentDirPath, exist_ok=True)
    klassenContentDirPath = os.path.join(berichtContentDirPath, 'klassen')
    os.makedirs(klassenContentDirPath, exist_ok=True)
    # bericht-index-bestand
    berichtContentIndexFilePath = os.path.join(berichtContentDirPath, '_index.md')
    with open(berichtContentIndexFilePath, 'w') as berichtContentIndexFile:
        print(buildFrontMatterStr({
            'bericht' : bericht.naam.upper(),
            'bookCollapseSection': True
        }), file=berichtContentIndexFile)
        print(generatieStempel(), file=berichtContentIndexFile)
    # klassen-index-bestand
    klassenContentIndexFilePath = os.path.join(klassenContentDirPath, '_index.md')
    with open(klassenContentIndexFilePath, 'w') as klassenContentIndexFile:
        print(buildSectionIndexMdStr(title='Klassen', description=bericht.naam.upper()), file=klassenContentIndexFile)
    # klassen-bestanden
    for klasse in bericht.klassen:
        createIstdKlasseHugoBookContent(bericht.naam, klasse, klassenContentDirPath)

def createIstdBerichtenHugoBookContent(berichten: IstdBericht = [], contentPath: str = defaultContentDocsPath()):
    """Maak HUGO Book Theme Content voor iStandaard Berichten."""
    # initialisatie
    berichtenContentPath = os.path.join(contentPath, 'berichten')
    os.makedirs(berichtenContentPath, exist_ok=True)
    # berichten-index-bestand
    berichtenContentIndexFilePath = os.path.join(berichtenContentPath, '_index.md')
    with open(berichtenContentIndexFilePath, 'w') as berichtenContentIndexFile:
        print(buildSectionIndexMdStr(title='Berichten', description='PM'), file=berichtenContentIndexFile)
    # bericht-bestanden
    for bericht in berichten:
        createIstdBerichtHugoBookContent(bericht, berichtenContentPath)

