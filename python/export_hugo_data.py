#
# Exporteren van HUGO Data in YAML-formaat vanuit iStandaard-klassen (istd_klassen)
#
# Status: Verkenning op basis van huidige (vanuit BizzDesign-scripts) gepubliceerde XSDs
#
# Auteur: A. Haldar (Onno)
#

# package imports
import os

# local imports
from istd_klassen import *
from yaml_utils import *

def defaultDataPath(rootPath: str = ''):
    """Standaard HUGO SSG Data Locatie."""
    return os.path.join(rootPath, 'data')

def exportCodeLijstYamls(codeLijsten: IstdCodeLijst = [], exportPath: str = defaultDataPath()):
    """Exporteer codelijsten in YAML-formaat."""
    # initialisatie
    codelijstPath = os.path.join(exportPath, 'codelijst')
    os.makedirs(codelijstPath, exist_ok=True)
    # schrijf YAML-bestand
    for codeLijst in codeLijsten:
        codeLijstFilePath = os.path.join(codelijstPath, codeLijst.naam + '.yml')
        with open(codeLijstFilePath, 'w') as codeLijstYaml:
            print(buildYamlStr(codeLijst), file=codeLijstYaml)

def exportRegelYamls(regels: IstdRegel = [], exportPath: str = defaultDataPath()):
    """Exporteer regels in YAML-formaat."""
    # initialisatie
    regelPath = os.path.join(exportPath, 'regel')
    os.makedirs(regelPath, exist_ok=True)
    # schrijf YAML-bestand
    for regel in regels:
        # schrijf regel directory "type" / bestand "code"
        regelTypePath = os.path.join(regelPath, regel.type)
        os.makedirs(regelTypePath, exist_ok=True)
        regelFilePath = os.path.join(regelTypePath, regel.code + '.yml')
        with open(regelFilePath, 'w') as regelYaml:
            print(buildYamlStr(regel), file=regelYaml)

def exportBasisSchemaYaml(basisSchema: IstdBasisSchema, exportPath: str = defaultDataPath()):
    """Exporteer Basisschema Release-informatie over de Data Typen in YAML-formaat."""
    # initialisatie
    basisSchemaPathFilePath = os.path.join(exportPath, 'basisschema.yml')
    os.makedirs(exportPath, exist_ok=True)
    # schrijf YAM-bestand
    with open(basisSchemaPathFilePath, 'w') as basisSchemaYaml:
        print(buildYamlStr(basisSchema), file=basisSchemaYaml)

def exportDataTypenYamls(dataTypen: IstdDataType = [], exportPath: str = defaultDataPath()):
    """Exporteer datatypen in YAML-formaat."""
    # initialisatie
    dataTypePath = os.path.join(exportPath, 'datatype')
    os.makedirs(dataTypePath, exist_ok=True)
    # schrijf YAML-bestand
    for dataType in dataTypen:
        dataTypeFilePath = os.path.join(dataTypePath, dataType.naam + '.yml')
        with open(dataTypeFilePath, 'w') as dataTypeYaml:
            print(buildYamlStr(dataType), file=dataTypeYaml)
    
def exportBerichtYaml(bericht: IstdBericht, exportPath: str = defaultDataPath()):
    """Exporteer Bericht in YAML-formaat."""
    # initialisatie
    berichtDataPath = os.path.join(exportPath, 'bericht')
    os.makedirs(berichtDataPath, exist_ok=True)
    berichtDataFilePath = os.path.join(berichtDataPath, bericht.naam + '.yml')
    # schrijf YAML-bestand
    with open(berichtDataFilePath, 'w') as berichtYaml:
        print(buildYamlStr(bericht), file=berichtYaml)
    

def exportBerichtenYamls(berichten: IstdBericht = [], exportPath: str = defaultDataPath()):
    """Exporteer Berichten in YAML-formaat."""
    # Exporteer Berichten
    for bericht in berichten:
        exportBerichtYaml(bericht, exportPath)
