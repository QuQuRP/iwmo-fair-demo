#
# iStandaard BizzDesign Output XSD-bestand (XLM-bericht schema) conversie naar Pyhon Class Formaat (istd_klassen)
#
# Status: Verkenning op basis van huidige (vanuit BizzDesign-scripts) gepubliceerde XSDs
#
# Auteur: A. Haldar (Onno)
#

# package imports
from pathlib import Path
import xmltodict

# local imports
from istd_klassen import IstdBericht, IstdAggregatieRelatie, IstdKlasse, IstdElement, IstdDataType, IstdBasisSchema

def xmlFileToDict(xmlFilePath: str):
    """Geeft XML-bestand terug als JSON-achtige Pyhon Dict-structuur terug."""
    xmlLines = ''
    with open(xmlFilePath) as xmlFile:
        for xmlLine in xmlFile.readlines():
            xmlLines = xmlLines + xmlLine + '\n'
    return xmltodict.parse(xmlLines)

def getXmlDictType(xmlPart: dict, partKey):
    """Geeft xml part dict-type terug."""
    return str((type(xmlPart[partKey])))

def getXmlObjectValue(xmlObject: dict, valuePrefix: str, ValueItem: str):
    """Geeft xml string-waarde waarde terug."""
    return xmlObject[valuePrefix + ':' + ValueItem]

def getDocStr(xsObject: dict):
    """Geeft string met Annontatie Documentatie Regel(s) van Xs-Object terug."""
    xsAnnotation = xsObject['xs:annotation']
    docStr: str = []
    if isinstance((xsAnnotation['xs:documentation']), list):
        # Meerdere Documentatie-regels
        for docLine in xsAnnotation['xs:documentation']:
            docStr.append(docLine)

    else:
        # 1 Documentatie-regel
        docStr.append(xsAnnotation['xs:documentation'])

    return docStr

def mergeRelatie(relaties: IstdAggregatieRelatie = [], relatieNaam: str = ''):
    """Zoek een relatie in de lijst (of voeg deze toe indien niet gevonden) en geef lijst-index daarvan terug."""
    # Zoek naar reeds toegevoegde relatie
    relatieIndex = -1
    i = -1
    for relatie in relaties:
        i = i + 1
        if relatie.naam == relatieNaam:
            # relatie gevonden
            relatieIndex = i
            
    if relatieIndex == -1:
        # Voeg relatie toe indien niet gevonden
        relaties.append(IstdAggregatieRelatie(relatieNaam))
        relatieIndex = len(relaties) - 1

    return relatieIndex

def addKlasseElement(bericht: IstdBericht, klasseNaam: str, klasseBeschrijving: str = None, elementNaam: str = None):
    """Voeg (Klasse) Element toe aan IstdBericht-Instantie."""
    # Haal index laatst toegevoegde Klasse op (of -1 indien nog geen Klassen toegevoegd)
    klasseIndex = -1
    i = 0
    while i < len(bericht.klassen) and klasseIndex == -1:
        if bericht.klassen[i].naam == klasseNaam:
            klasseIndex = i
        else:
            i = i + 1
    
    if klasseIndex == -1:
        # Volgende Klasse toevoegen
        bericht.klassen.append(IstdKlasse(klasseNaam))
        klasseIndex = i
        bericht.klassen[klasseIndex].naam = klasseNaam
        bericht.klassen[klasseIndex].beschrijving = klasseBeschrijving

    # Volgende Element toevoegen
    bericht.klassen[klasseIndex].elementen.append(IstdElement(elementNaam))
    elementIndex = len(bericht.klassen[klasseIndex].elementen) - 1

    return klasseIndex, elementIndex
    
def addBerichtKlasse(xsKlasse: dict, bericht: IstdBericht):
    """Voeg op basis van de xsKlasse een IstdBericht-Instantie toe met de Elementen en Relaties."""
    # Initialisatie
    klasseNaam = str(xsKlasse['@name'])
    klasseBeschrijving = '(geen beschrijving gevonden)'

    if 'xs:annotation' in xsKlasse:
        # Voeg Klassen-beschrijving toe indien aanwezig
        klasseBeschrijving = getDocStr(xsKlasse)
        if klasseNaam == 'Root':
            bericht.beschrijving = klasseBeschrijving

    # Mapping van waarden in de Klasse-Sequence
    xsSeq = xsKlasse['xs:sequence']
    relatiePrefix =  bericht.naam.lower()
    elementPrefix = bericht.standaard.lower()

    for seqKey in xsSeq:
        # Een Sequence kan meerdere (type => list) 
        # of 1 Element bevatten (type => collections.OrderedDict)
        dictType = getXmlDictType(xsSeq, seqKey)
        if dictType == "<class 'list'>":
            # Meerdere Elementen in de Sequence
            for xsElement in xsSeq[seqKey]:
                xsElementName = str(xsElement['@name'])

                if '@type' in xsElement:
                    # Een Klasse-Element heeft een Type-aanduiding
                    xsElementType = str(xsElement['@type'])

                    if xsElementType.startswith(relatiePrefix + ':'):
                        # Een Relatie-Element heeft een Type-prefix met de <Bericht-naam> (bijv. "iwmo305:...")
                        i = mergeRelatie(bericht.relaties, xsElementName)
                        parentKlasse = xsKlasse['@name']
                        bericht.relaties[i].parentKlasse = parentKlasse
                        if parentKlasse == 'Root':
                            # Root-Klasse bevat alleen directe relaties met de Header en een inhoudelijke Klasse
                            bericht.relaties[i].childKlasse = xsElementName
                        if '@minOccurs' in xsElement:
                            bericht.relaties[i].childMin = xsElement['@minOccurs']
                        
                    elif xsElementType.startswith(elementPrefix + ':'):
                        # Een Element zonder vaste waarde heeft een Type-prefix met de <Standaard> (bijv. "iwmo:...")
                        klasseIndex, elementIndex = addKlasseElement(bericht, klasseNaam, klasseBeschrijving, xsElementName)
                        
                        if 'xs:annotation' in xsElement:
                            bericht.klassen[klasseIndex].elementen[elementIndex].beschrijving = getDocStr(xsElement)
                        else:
                            print('Waarschuwing - Geen xs:annotation - gevonden!')
                            print('bericht.klasse: ' + bericht.klassen[klasseIndex].naam)
                            print('bericht.klasse.element: ' + str(xsElement))

                        bericht.klassen[klasseIndex].elementen[elementIndex].dataType = xsElementType.split(':')[1]

                    else:
                        # Deze situatie is onbekend??
                        print('Onbekend Element Type in Bericht : "' + bericht.naam + '"')
                        print('<xs:element type+"' + xsElementType + '">')

                else:
                    # Een Element met een vaste Waarde heeft GEEN Type-aanduiding
                    xsSimpleType = xsElement['xs:simpleType']
                    xsRestriction = xsSimpleType['xs:restriction']
                    xsDataType = str(xsRestriction['@base'])
                    xsPattern = xsRestriction['xs:pattern']
                    xsWaarde = str(xsPattern['@value'])
                    klasseIndex, elementIndex = addKlasseElement(bericht, klasseNaam, klasseBeschrijving, xsElementName)
                    bericht.klassen[klasseIndex].elementen[elementIndex].beschrijving = getDocStr(xsElement)
                    bericht.klassen[klasseIndex].elementen[elementIndex].dataType = xsDataType.split(':')[1]
                    bericht.klassen[klasseIndex].elementen[elementIndex].waarde = xsWaarde


        elif dictType == "<class 'collections.OrderedDict'>":
            # Een Sequence met 1 Element is een Klasse-Relatie Element
            relatieNaam = str(xsKlasse['@name'])
            i = mergeRelatie(bericht.relaties, relatieNaam)
            relatieElement = xsSeq['xs:element']
            bericht.relaties[i].childKlasse = relatieElement['@name']
            if '@maxOccurs' in relatieElement:
                bericht.relaties[i].childMax = relatieElement['@maxOccurs']

        else:
            print('Onbekende dictType:' + dictType)


def importBerichtXsd(berichtXsdFilePath : str):
    """Importeerd XSD-Bericht als IstdBericht-Class."""

    # Lees Bericht-gegevens in Dict-structuur Object
    bericht = IstdBericht(Path(berichtXsdFilePath).stem)
    xmlDictObject = xmlFileToDict(berichtXsdFilePath)

    # Lees XSD-Schema
    schema = xmlDictObject['xs:schema']

    # Converteer XSD-AppInfo waarden
    appInfo = schema['xs:annotation']['xs:appinfo']
    appInfoValuesView = appInfo.values()
    appInfoValueIterator = iter(appInfoValuesView)
    iStdPrefix = next(appInfoValueIterator)
    bericht.standaard = getXmlObjectValue(appInfo, iStdPrefix, 'standaard')
    bericht.release = getXmlObjectValue(appInfo, iStdPrefix, 'release')
    bericht.berichtXsdVersie = getXmlObjectValue(appInfo, iStdPrefix, 'BerichtXsdVersie')
    bericht.berichtXsdMinVersie = getXmlObjectValue(appInfo, iStdPrefix, 'BerichtXsdMinVersie')
    bericht.berichtXsdMaxVersie = getXmlObjectValue(appInfo, iStdPrefix, 'BerichtXsdMaxVersie')
    bericht.basisschemaXsdVersie = getXmlObjectValue(appInfo, iStdPrefix, 'BasisschemaXsdVersie')
    bericht.basisschemaXsdMinVersie = getXmlObjectValue(appInfo, iStdPrefix, 'BasisschemaXsdMinVersie')
    bericht.basisschemaXsdMaxVersie = getXmlObjectValue(appInfo, iStdPrefix, 'BasisschemaXsdMaxVersie')

    # Converteer XSD-ComplexType waarden voor Bericht-Klassen
    for xsKlasse in schema['xs:complexType']:        
        addBerichtKlasse(xsKlasse, bericht)

    # Koppel nog niet gelegde Relaties
    for relatie in bericht.relaties:

        if relatie.childKlasse == None:
            # Relatie zonder tussen KLasse-Schakel (1-op-1)
            relatie.childKlasse = relatie.naam
        elif relatie.parentKlasse == None:
            # Uitzondering niet goed gelegde Relatie naar parent
            relatie.childKlasse = relatie.naam

            for parentRelatie in bericht.relaties:

                if parentRelatie.childKlasse == relatie.naam:
                    relatie.parentKlasse = parentRelatie.childKlasse

        
    return bericht


def addComplexType(xsComplexType: dict, dataTypen: IstdDataType = []):
    """Voeg op basis van een xsComplexType een IstdDataType-Instantie toe (met de Elementen)."""
    dataTypen.append(IstdDataType(str(xsComplexType['@name'])))
    d = len(dataTypen) - 1

    if 'xs:annotation' in xsComplexType:
        dataTypen[d].beschrijving = getDocStr(xsComplexType)

    # Mapping van waarden in de Klasse-Sequence
    xsSeq = xsComplexType['xs:sequence']

    for seqKey in xsSeq:
        # Een Sequence kan meerdere (type => list) 
        # of 1 Element bevatten (type => collections.OrderedDict)
        dictType = getXmlDictType(xsSeq, seqKey)
        if dictType == "<class 'list'>":
            # Meerdere Elementen in de Sequence
            for xsElement in xsSeq[seqKey]:
                xsElementName = str(xsElement['@name'])
                dataTypen[d].elementen.append(IstdElement(xsElementName))
                e = len(dataTypen[d].elementen) - 1

                if '@type' in xsElement:
                    # Element zonder restrictie
                    xsElementDataType = str(xsElement['@type'])
                else:
                    # Element met restrictie
                    xsSimpleType = xsElement['xs:simpleType']
                    xsRestriction = xsSimpleType['xs:restriction']
                    xsDataType = str(xsRestriction['@base'])
                    xsElementDataType = xsDataType

                    if 'xs:pattern' in xsRestriction:
                        dataTypen[d].elementen[e].waarde = str(xsRestriction['xs:pattern']['@value'])
                    elif 'xs:maxInclusive' in xsRestriction:
                        dataTypen[d].elementen[e].maxWaarde = str(xsRestriction['xs:maxInclusive']['@value'])
                    
                dataTypen[d].elementen[e].dataType = xsElementDataType.split(':')[1]

                if 'xs:annotation' in xsElement:
                    dataTypen[d].elementen[e].beschrijving = getDocStr(xsElement)

                if '@minOccurs' in xsElement:
                     dataTypen[d].elementen[e].verplicht = str(xsElement['@minOccurs']) != '0'

        elif dictType == "<class 'collections.OrderedDict'>":
            # Een Sequence met 1 Element is een Klasse-Relatie Element
            print('en Sequence met 1 Element')
            print(xsSeq[seqKey])

def addSimpleType(xsSimpleType: dict, dataTypen: IstdDataType = []):
    """Voeg op basis van een xsSimpleType een IstdDataType-Instantie toe."""
    dataTypen.append(IstdDataType(str(xsSimpleType['@name'])))
    d = len(dataTypen) - 1

    if 'xs:annotation' in xsSimpleType:
        dataTypen[d].beschrijving = getDocStr(xsSimpleType)

    xsRestriction = xsSimpleType['xs:restriction']
    xsRestrictionBase: str = xsRestriction['@base']
    dataTypen[d].basisType = xsRestrictionBase.split(':')[1]

    if 'xs:minLength' in xsRestriction:
        dataTypen[d].minLengte = xsRestriction['xs:minLength']['@value']
    if 'xs:maxLength' in xsRestriction:
        dataTypen[d].maxLengte = xsRestriction['xs:maxLength']['@value']
    if 'xs:minInclusive' in xsRestriction:
        dataTypen[d].minWaarde = xsRestriction['xs:minInclusive']['@value']
    if 'xs:maxInclusive' in xsRestriction:
        dataTypen[d].maxWaarde = xsRestriction['xs:maxInclusive']['@value']
    if 'xs:pattern' in xsRestriction:
        dataTypen[d].waarde = xsRestriction['xs:pattern']['@value']



def importBasisschemaXsd(basisschemaXsdFilePath : str):
    """Importeerd XSD-basisschema als IstdDataType-Class Lijst."""

    # Lees Basisschema-gegevens in Dict-structuur Object
    dataTypen: IstdDataType = []
    xmlDictObject = xmlFileToDict(basisschemaXsdFilePath)

    # Lees XSD-Schema
    schema = xmlDictObject['xs:schema']

    # Converteer XSD-AppInfo waarden
    appInfo = schema['xs:annotation']['xs:appinfo']
    appInfoValuesView = appInfo.values()
    appInfoValueIterator = iter(appInfoValuesView)
    iStdPrefix = next(appInfoValueIterator)
    basisSchema = IstdBasisSchema(getXmlObjectValue(appInfo, iStdPrefix, 'standaard'))
    basisSchema.release = getXmlObjectValue(appInfo, iStdPrefix, 'release')
    basisSchema.xsdVersie = getXmlObjectValue(appInfo, iStdPrefix, 'BasisschemaXsdVersie')
    basisSchema.xsdMinVersie = getXmlObjectValue(appInfo, iStdPrefix, 'BasisschemaXsdMinVersie')
    basisSchema.xsdMaxVersie = getXmlObjectValue(appInfo, iStdPrefix, 'BasisschemaXsdMaxVersie')
    
    for xsComplexType in schema['xs:complexType']:
        addComplexType(xsComplexType, dataTypen)

    for xsSimpleType in schema['xs:simpleType']:
        addSimpleType(xsSimpleType, dataTypen)

    return basisSchema, dataTypen