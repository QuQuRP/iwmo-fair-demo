#
# iStandaard Klassen voor de conversie van het Informatiemodel van BizzDesign naar Open Formaten
#
# Status: Verkenning op basis van huidige (vanuit BizzDesign-scripts) gepubliceerde XSDs
#
# Auteur: A. Haldar (Onno)
#

# Basisschema
# - beschrijving van release DataTypen
class IstdBasisSchema:
    def __init__(self, standaard: str):
        self.standaard = standaard
        self.release = 'x.y.z'
        self.xsdVersie = 'x.y.z'
        self.xsdMinVersie = 'x.y.z'
        self.xsdMaxVersie = 'x.y.z'            

# DataType:
# - kan meerdere elementen bevatten
# - kan worden beperkt door een codelijst
# - en er kunnen meerdere regels voor gelden
class IstdDataType:
    """iStandaard Bericht Data Type Conversie Klasse"""
    def __init__(self, naam: str):
        self.naam: str = naam
        """ XSD => ['xs:...Type']['@name'] """
        self.beschrijving: str = []
        """ XSD => ['xs:...Type']['xs:annotation']['xs:documentation'] """
        self.elementen: IstdElement = []
        """ XSD => [...]['xs:sequence']['xs:element'] """
        self.basisType: str = None
        """ XSD => ['xs:simpleType']['xs:restriction']['@base'] """
        self.maxLengte: int = None
        """ XSD => ['xs:simpleType']['xs:restriction']['xs:maxLength']['@value'] """
        self.minLengte: int = None
        """ XSD => ['xs:simpleType']['xs:restriction']['xs:minLength']['@value'] """
        self.minWaarde = None 
        """ XSD => ['xs:simpleType']['xs:restriction']['xs:minInclusive']['@value'] """
        self.maxWaarde = None 
        """ XSD => ['xs:simpleType']['xs:restriction']['xs:maxInclusive']['@value'] """
        self.waarde: int = None
        """ XSD => ['xs:simpleType']['xs:restriction']['xs:pattern']['@value'] """
        self.codeLijst: str = None
        self.regels: str = []

# Element:
# - heeft een datatype
# - kan wel of geen sleutel zijn
# - en er kunnen meerdere regels voor gelden
class IstdElement:
    """iStandaard Bericht Element Conversie Klasse"""
    def __init__(self, naam: str):
        self.naam: str = naam
        """ XSD => ['xs:element']['@name'] """
        self.beschrijving: str = []
        """ XSD => ['xs:element']['xs:annotation']['xs:documentation'] """
        self.dataType: str = None
        self.verplicht = bool = None
        """ XSD => ['xs:element']['@inOccurs'] != '0' """
        self.waarde = None
        """ XSD => ['xs:element']['xs:pattern']['@value'] """
        self.maxWaarde = None 
        """ XSD => ['xs:element']['xs:maxInclusive']['@value'] """
        self.sleutel: bool = None
        self.regels: str = []

# Klasse:
# - bestaat uit meerdere elementen
# - en er kunnen meerdere regels voor gelden
class IstdKlasse:
    def __init__(self, naam: str):
        self.naam: str = naam
        self.beschrijving: str = []
        self.regels: str = []
        self.elementen: IstdElement = []

# Aggregatie Relatie
# - Is van parent-Klasse naar child-Klasse
# - Geeft Min - Max voorkomens van de child-Klasse aan (default = 1 - 1)
class IstdAggregatieRelatie:
    def __init__(self, naam: str):
        self.naam: str = naam
        self.parentKlasse: str = None
        self.childKlasse: str = None
        self.childMin: str = '1'
        self.childMax: str = '1'

# Bericht:
# - Bestaat uit meerdere klassen 
# - Kunnen meerdere regels voor gelden
# - Meerdere aggregatie-relaties tussen klassen
class IstdBericht:
    def __init__(self, naam: str):
        self.naam: str = naam.upper()
        self.standaard = 'istd'
        self.release = 'x.y.z'
        self.berichtXsdVersie = 'x.y.z'
        self.berichtXsdMinVersie = 'x.y.z'
        self.berichtXsdMaxVersie = 'x.y.z'
        self.basisschemaXsdVersie = 'x.y.z'
        self.basisschemaXsdMinVersie = 'x.y.z'
        self.basisschemaXsdMaxVersie = 'x.y.z'
        self.beschrijving: str = []
        self.regels: str = []
        self.klassen: IstdKlasse = []
        self.relaties: IstdAggregatieRelatie = []

# Regel:
# - is uniek binnen de verzameling regels
# - is optioneel gekoppeld aan referentie-regel
class IstdRegel:
    def __init__(self):
        self.type: str = None
        self.code: str = None
        self.titel: str = None
        self.documentatie: str = None
        self.XSDRestrictie = None
        self.retourcode = None
        self.controleniveau = None
        self.referentieregel = None

# Codelijst Item
class IstdCodeItem:
    def __init__(self):
        self.code = None
        self.documentatie = None
        self.mutatiedatum: str = None
        self.mutatie: str = None
        self.ingangsdatum: str = None
        self.expiratiedatum: str = None

# Codelijst
class IstdCodeLijst:
    def __init__(self, naam: str, beschrijving: str):
        self.naam = naam
        self.beschrijving = beschrijving
        self.documentatie = None
        self.codeItems: IstdCodeItem = []

