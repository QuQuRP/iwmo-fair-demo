import yaml

def buildYamlStr(object: any):
    yamlDump: str = yaml.dump(object)
    yamLines: str = ''

    for line in yamlDump.split('\n'):
        nextLine: str = str(line)
        test = nextLine.strip()

        if test.startswith('!!') or test.startswith('- !!'):
            # Volgende regel is een Commentaar-regel
            yamLines = yamLines + nextLine.replace('!!', '# ', 1) + '\n'

        elif not (test.endswith('null') or test.endswith('[]')):
            # Volgende regels is NIET Leeg
            yamLines = yamLines + nextLine + '\n'

    return yamLines
